        google.load("visualization", "1");

        // Set callback to run when API is loaded
        google.setOnLoadCallback(drawVisualization);

        var timeline;
        var data;

        // Called when the timelineualization API is loaded.
        function drawVisualization() {
            // Create and populate a data table.
            data = new google.visualization.DataTable();
            data.addColumn('datetime', 'start');
            data.addColumn('datetime', 'end');
            data.addColumn('string', 'content');

            function addRow(startDate, endDate, content, backgroundColor, borderColor)
            {
                // we make our own customized layout for the events

                if (backgroundColor == undefined)
                    backgroundColor = "yellow";
                if (borderColor == undefined)
                    borderColor = "gold";

                var fill = endDate ? "pink" : "yellow";
                var div = '<div style="background-color:' + backgroundColor +
                        '; border:1px solid ' + borderColor + ';padding:5px;">' +
                        content + '</div>';

                data.addRows([
                    [startDate, endDate, div]
                ]);
            }

            data.addRows([
                [new Date(2013,0,5,11,10), ,'Tag 1.'],
		[new Date(2013,0,5,11,20), ,'Tag 2.'],
		[new Date(2013,0,5,11,30), ,'Tag 3.'],
		[new Date(2013,0,5,11,40), ,'Tag 4.'],
		[new Date(2013,0,5,11,50), ,'Tag 5.'],
		[new Date(2013,0,5,11,15), ,'Tag 11.'],
		[new Date(2013,0,5,11,25), ,'Tag 12.'],
		[new Date(2013,0,5,11,35), ,'Tag 13.'],
		[new Date(2013,0,5,11,45), ,'Tag 14.'],
		[new Date(2013,0,5,11,55), ,'Tag 15.'],
		[new Date(2013,0,5,11,12), ,'Tag 21.'],
		[new Date(2013,0,5,11,22), ,'Tag 22.'],
		[new Date(2013,0,5,11,23), ,'New Tag.'],
		[new Date(2013,0,5,11,34), ,'Tag 23.'],
		[new Date(2013,0,5,11,43), ,'Tag 24.'],
		[new Date(2013,0,5,11,52), ,'Tag 25.'],
		[new Date(2013,0,5,11,11), ,'Tag 6.'],
		[new Date(2013,0,5,11,22), ,'Tag 7.'],
		[new Date(2013,0,5,11,33), ,'Tag 8.'],
		[new Date(2013,0,5,11,44), ,'Tag 9.'],
		[new Date(2013,0,5,11,57), ,'Tag 10.'],
		[new Date(2013,0,5,11,13), ,'Tag 16.'],
		[new Date(2013,0,5,11,23), ,'Tag 17.'],
		[new Date(2013,0,5,11,34), ,'Tag 18.'],
		[new Date(2013,0,5,11,46), ,'Tag 19.'],
		[new Date(2013,0,5,11,53), ,'Tag 20.']
            ]);

            // specify options
            var options = {
                width:     "90%",
                height:    "auto",
                start: new Date(2013,0,5,11,0),
                end:   new Date(2013,0,5,12,0),
                style:    "box"    // optional. choose "dot" (default), "box", or "none"
            };

            // Instantiate our table object.
            timeline = new links.Timeline(document.getElementById('mytimeline'));

            // Attach event listeners
            google.visualization.events.addListener(timeline, 'select', onselect);
            google.visualization.events.addListener(timeline, 'rangechange', onrangechange);

            // Draw our table with the data we created locally.
            timeline.draw(data, options);

            // Set the scale by hand. Autoscaling will be disabled.
            // Note: you can achieve the same by specifying scale and step in the
            // options for the timeline.
            //timeline.setScale(links.Timeline.StepDate.SCALE.DAY, 1);
        }

        // adjust start and end time.
        function setTime() {
            if (!timeline) return;

            var newStartDate = new Date(document.getElementById('startDate').value);
            var newEndDate   = new Date(document.getElementById('endDate').value);
            timeline.setVisibleChartRange(newStartDate, newEndDate);
            timeline.redraw();
        }

        function onrangechange() {
            // adjust the values of startDate and endDate
            var range = timeline.getVisibleChartRange();
            document.getElementById('startDate').value = dateFormat(range.start);
            document.getElementById('endDate').value   = dateFormat(range.end);
        }
	
	

        function onselect() {
            var sel = timeline.getSelection();
            if (sel.length) {
                if (sel[0].row != undefined) {
                    var row = sel[0].row;
                       hb = new HelpBalloon({
			title: '<Tag Clicked>',
			content: '<Tag Desription >\n' + 'is is an example of static '
			+ 'balloon content.',
			icon: $('tagInfo'),
			anchorPosition: 'centre right'
			});
		    hb.show();
                }
            }
        }

        // Format given date as "yyyy-mm-dd hh:ii:ss"
        // @param datetime   A Date object.
        function dateFormat(date) {
            datetime =   date.getFullYear() + "-" +
                    ((date.getMonth()   <  9) ? "0" : "") + (date.getMonth() + 1) + "-" +
                    ((date.getDate()    < 10) ? "0" : "") +  date.getDate() + " " +
                    ((date.getHours()   < 10) ? "0" : "") +  date.getHours() + ":" +
                    ((date.getMinutes() < 10) ? "0" : "") +  date.getMinutes() + ":" +
                    ((date.getSeconds() < 10) ? "0" : "") +  date.getSeconds()
            return datetime;
        }


