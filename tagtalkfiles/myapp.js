(function() {
// define namespace
myapp = {};

// all core modules
include('frameworks/uki/uki-core.js');

// used views, comment out unused ones
include('frameworks/uki/uki-view/view/box.js');
include('frameworks/uki/uki-view/view/image.js');
include('frameworks/uki/uki-view/view/button.js');
include('frameworks/uki/uki-view/view/checkbox.js');
include('frameworks/uki/uki-view/view/radio.js');
include('frameworks/uki/uki-view/view/textField.js');
include('frameworks/uki/uki-view/view/label.js');
include('frameworks/uki/uki-view/view/list.js');
include('frameworks/uki/uki-view/view/table.js');
include('frameworks/uki/uki-view/view/slider.js');
include('frameworks/uki/uki-view/view/splitPane.js');
include('frameworks/uki/uki-view/view/scrollPane.js');
include('frameworks/uki/uki-view/view/popup.js');
include('frameworks/uki/uki-view/view/flow.js');
include('frameworks/uki/uki-view/view/toolbar.js');

// theme
include('frameworks/uki/uki-theme/airport.js');

// data
include('frameworks/uki/uki-data/model.js');
include('myapp/layout/main.js');


// skip interface creation if we're testing
if (window.TESTING) return;

//myapp.layout.main().attachTo(window, '100 100');

var html = '<img src="logo.png" width="24" height="24" style="position: absolute; top: 3px; left: -26px;" /> Icon';

/* The code to create the tool bar 
* this just creates the buttons to be displayed */ 
uki(
    { view:'Toolbar', rect: '55 117 206 25', anchors: 'left', background: 'white', buttons: [
                {view: 'Button', rect: '10 0 200 28', anchors: 'left top', text: 'Home', id: 'Home'},
		{view: 'Button', rect: '10 0 200 28', anchors: 'left top', text: 'My Courses', id: 'Courses'},
		{view: 'Button', rect: '10 0 200 28', anchors: 'left top', text: 'Help', id: 'Help'},
	        {view: 'Button', rect: '10 0 200 28', anchors: 'left top', text: 'Contact', id:'Contact'}]}        
).attachTo( window );


/* BEGIN SECTION HANDLING OF TOOLBAR NAVIGATION BUTTONS */

/* Code to create the pop up for the menu items as they are clicked */

/* HOME BUTTON */
    uki(
    { view: 'Popup', rect: '0 0 100 50', anchors: 'left top', relativeTo: uki('#Home')[0], id: 'test', text: 'Hello!!' }
    );

    uki('#Home').click(function() {
    	//uki('#test').toggle();
        alert('Hello world!')
    });


/* END SECTION */
})();
