/** This ile contains the java script that is used to populate the tables
* for both the available series an the available lectures **/

function addRow(url, title, description)
{	

	//href='tagtalkfiles/tagtalk.html?url=/podcasting/event/s1'
         tabBody=document.getElementsByTagName("TBODY").item(0);
         row=document.createElement("TR");
         cell1 = document.createElement("TD");
	 cell3 = document.createElement("TD");
	 
	 //create the link to the webpage	 
	 var a = document.createElement('a');
	 a.title = title;
	 a.innerHTML = a.title;
	 a.href = 'tagtalkfiles/coursepage.html?url='+url;
         
         textnode3=document.createTextNode(description);

         cell1.appendChild(a);
	 cell3.appendChild(textnode3);

         row.appendChild(cell1);
	 row.appendChild(cell3);

         tabBody.appendChild(row);
}

function makeRequest(url) {
	//this creates the get response and handles it
	//it uses the results to fill in the table
        var http_request = false;

        if (window.XMLHttpRequest) { // Mozilla, Safari,...
            http_request = new XMLHttpRequest();
            if (http_request.overrideMimeType) {
                http_request.overrideMimeType('text/xml');
                // See note below about this line
            }
        } else if (window.ActiveXObject) { // IE
            try {
                http_request = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                try {
                    http_request = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (e) {}
            }
        }

        if (!http_request) {
            alert('Giving up :( Cannot create an XMLHTTP instance');
            return false;
        }
        http_request.onreadystatechange = function() { alertContents(http_request); };
        http_request.open('GET', url, true);
    	http_request.setRequestHeader("Content-Type", "text/plain");
    	http_request.setRequestHeader("Accept", "application/json");
        http_request.send(null);

    }

    function alertContents(http_request) {

        if (http_request.readyState == 4) {
            if (http_request.status == 200) {
		var series = http_request.responseText;
		//we now parse the string using string.split()
		//I can't get it to parse with jquery.parseJSON 
		//so I shall try getting this working later

		//the string is in the format 
		// { event: [ list of all the events]
		//so we split on the string "id" to get an array of all the events

		var all=series.split("id");
			
		for (i=1;i<all.length;i++) {
			//for each of the events get the url, title, description and start date
			//pass this to the function add row which adds them to the table
						
			var temp = all[i].split(': "');
			
			//alert(temp.length);

			var urlRaw = temp[2].split('",');
			var url = urlRaw[0]; //get the url
			
			var titleRaw = temp[3].split('",');
			var title = titleRaw[0];//get the title
			
			var descriptionRaw = titleRaw[1].split(':');
			var dR = descriptionRaw[1].split(',');

			var description = dR[0]; //get the description

			//handle when the description is false
			if (description == false){
				description = "n/a";
			}
			

			//alert(description );
			//var sdRaw = temp[6].split('",');
			//var startdate = sdRaw[0];//get startdate		
		
			//alert("url = " +url + " title = " + title + " descrip " + description);
			addRow(url, title, description);
		}
		
            } else {
                alert('There was a problem with the request.');
            }
        }

    }

 
