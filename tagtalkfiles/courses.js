/** This ile contains the java script that is used to populate the tables
* for both the available series an the available lectures **/

var seriesTitle; //so we can pass this to the other file
var seriesUrl; //so we can link back to this point

function makeRequest() {
	//this creates the get response and handles it
	//it uses the results to fill in the table
        var http_request = false;
	
	var s = window.location.search;
	var test =s.split("=");
	var url = test[1];
	seriesUrl=url;
	//alert (url);

        if (window.XMLHttpRequest) { // Mozilla, Safari,...
            http_request = new XMLHttpRequest();
            if (http_request.overrideMimeType) {
                http_request.overrideMimeType('text/xml');
                // See note below about this line
            }
        } else if (window.ActiveXObject) { // IE
            try {
                http_request = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                try {
                    http_request = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (e) {}
            }
        }

        if (!http_request) {
            alert('Giving up :( Cannot create an XMLHTTP instance');
            return false;
        }
        http_request.onreadystatechange = function() { getName(http_request); };
        http_request.open('GET', url, true);
    	http_request.setRequestHeader("Content-Type", "text/plain");
    	http_request.setRequestHeader("Accept", "application/json");
        http_request.send(null);

    }

/** BEGIN SECTION - HANDLERS FOR THE HTTP REQUESTS **/

    //called from the first request
    //gets the details of the given series
    //these then get a request to get all of the previous lectures
    function getName(http_request) {

        if (http_request.readyState == 4) {
            if (http_request.status == 200) {
		var series = http_request.responseText;
		//we now parse the string using string.split()
		//I can't get it to parse with jquery.parseJSON 
		//so I shall try getting this working later

		//the string is in the format 
		// { event: [ list of all attributes]
		//so we split on the string "id" to get an array of all the events
		
		var all=series.split("id");
				
		for (i=1;i<all.length;i++) {
			//for each of the attributes parse to get the title, escription and date
	
			var temp = all[i].split(': "');
			
			var titleRaw = temp[3].split('",');
			var title = titleRaw[0];//get the title
			seriesTitle = title;
			
			//alert(titleRaw[1]);

			var descriptionRaw = titleRaw[1].split(',');
			var dR = descriptionRaw[0].split(':'); //get the description
			var description = dR[1];

			//var sdRaw = temp[6].split('",');
			//var startdate = sdRaw[0];//get startdate		
		
			//alert("url = " +url + " title = " + title + " descrip " + description);
			//addRow(url, title, description);
			
			//fill the html content with this info
			addText(title, description);

			//make a new request to get the lectures in this series
			var newUrl = "/podcasting/track/" + title;
			
			http_request.onreadystatechange = function() { getLectures(http_request); };
        		http_request.open('GET', newUrl, true);
    			http_request.setRequestHeader("Content-Type", "text/plain");
    			http_request.setRequestHeader("Accept", "application/json");
        		http_request.send(null);
			
		}
		
            } else {
                alert('There was a problem with the request.');
            }
        }

    }
     
    //this is called from the second request
    //it get the list of available lectures
    function getLectures(http_request) {

        if (http_request.readyState == 4) {
            if (http_request.status == 200) {
		var lectures = http_request.responseText;
		//we now parse the string using string.split()
		//I can't get it to parse with jquery.parseJSON 
		//so I shall try getting this working later

		//the string is in the format 
		// { event: [ list of all the lecturess]
		//so we split on the string "id" to get an array of all the lectures


		//alert(lectures);
		var getlectures = lectures.split("events");
		var all=getlectures[1].split("id");

		//alert(all);
		
		for (i=1;i<all.length;i++) {
			//then parse this to get the lecture number, url, description and date
			//and add this to the table
			
			var temp = all[i].split(': "');
			
			var urlRaw = temp[3].split('",');
			var url = urlRaw[0]; //get the url

			var titleRaw = temp[4].split('",');
			var title = titleRaw[0];//get the title
			
			var descriptionRaw = temp[5].split('",');
			var description = descriptionRaw[0]; //get the description
			//alert(descriptionRaw.length);

			var sdRaw = descriptionRaw[1];
			// startdate = sdRaw[0];//get startdate
			//alert(sdRaw);	
		
			//alert("url = " +url + " title = " + title + " descrip " + description);
			addRow(url, title, description);
		}
		
				
	            } else {
                alert('There was a problem with the request.');
            }
        }

    }

/** END SECTION **/

/** BEGIN SECTION - HELPER METHODS **/
 /** Helper Methods **/

//add text to the page
function addText(title, description){

	var el = document.getElementById("coursename");
	el.innerHTML = title;

	var el = document.getElementById("courseinfo");
	el.innerHTML = description;
}

//this populates the table with available lectures in the given series
function addRow(url, title, description)
{	
	//alert("Adding row...." );

	//href='tagtalkfiles/tagtalk.html?url=/podcasting/event/s1'
         tabBody=document.getElementsByTagName("TBODY").item(0);
         row=document.createElement("TR");
         cell1 = document.createElement("TD");
         cell2 = document.createElement("TD");
	 cell3 = document.createElement("TD");
	 
	 //create the link to the webpage	 
	 var a = document.createElement('a');
	 a.title = title;
	 a.innerHTML = a.title;
	 var s = url + "=" + seriesTitle + "=" + seriesUrl;
	 //alert(s);
	 a.href = 'timeline.html?url='+s;

         textnode2=document.createTextNode(description);
         textnode3=document.createTextNode("Date TBC");

         cell1.appendChild(a);
         cell2.appendChild(textnode2);
	 cell3.appendChild(textnode3);

         row.appendChild(cell1);
         row.appendChild(cell2);
	 row.appendChild(cell3);

         tabBody.appendChild(row);
}

/** END SECTION **/
