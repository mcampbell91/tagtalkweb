/** This ile contains the java script that is used to populate the tables
* for both the available series an the available lectures **/

function addRow()
{	courses = ["Astronomy 1", "Astronomy 2", "Astronomy 3"];
	
	var allSeriesRaw = [["/podcasting/event/sDance", "Dance", "All Pom Dance Competition Music 2012", new Date(2010, 5,23,11,32, 0,0), "Norman Gray"], ["/podcasting/event/sCheer", "Cheer", "All Cheer Competition Music 2012", new Date(2010, 5,21,11,32, 0,0), "Norman Gray"]];

	//for the table columns are in order name, description, start date, administrator

	for (i=0;i<allSeriesRaw.length;i++) {
         tabBody=document.getElementsByTagName("TBODY").item(0);
         row=document.createElement("TR");
         cell1 = document.createElement("TD");
         cell2 = document.createElement("TD");
	 cell3 = document.createElement("TD");
	 cell4 = document.createElement("TD");
	 
	 //create the link to the webpage	 
	 var a = document.createElement('a');
	 a.title = allSeriesRaw[i][1];
	 a.innerHTML = a.title;
	 a.href = allSeriesRaw[i][1];

         //textnode1=document.createTextNode();
         textnode2=document.createTextNode(allSeriesRaw[i][2]);
         textnode3=document.createTextNode(allSeriesRaw[i][3]);
   	 textnode4=document.createTextNode(allSeriesRaw[i][4]);

         cell1.appendChild(a);
         cell2.appendChild(textnode2);
	 cell3.appendChild(textnode3);
	 cell4.appendChild(textnode4);

         row.appendChild(cell1);
         row.appendChild(cell2);
	 row.appendChild(cell3);
	 row.appendChild(cell4);

         tabBody.appendChild(row);
	}
}


function getSeriesList()
{
    var http_request = SimileAjax.XmlHttp._createRequest();
    if (!http_request) {
        alert('Giving up: Cannot create an XMLHTTP instance');
        return "false";
    }

    //http_request.onreadystatechange = function() {
      //  handlePostResponse(http_request, url, redisplay);
    //};
    http_request.open('GET',"/podcasting/track" , true);
    http_request.setRequestHeader("Content-Type", "text/plain");
    http_request.setRequestHeader("Accept", "application/json");
    var allSeries = eval("("+http_request.responseText+")");
    //console.log("Request: " + payload);
    //http_request.send(payload);
    return allSeries;
}

function makeAndHandlePOST()
{
    var http_request = SimileAjax.XmlHttp._createRequest();
    if (!http_request) {
        alert('Giving up: Cannot create an XMLHTTP instance');
        return false;
    }
    alert("created request successuflly.");

    http_request.onreadystatechange = function() {
        handlePostResponse(http_request, "/podcasting/track");
    };

    http_request.open('GET', "/podcasting/track", true);
    http_request.setRequestHeader("Content-Type", "text/plain");
    http_request.setRequestHeader("Accept", "application/json");
    //console.log("Request: " + payload);
    http_request.send();
}

function handlePostResponse(http_request, url)
{
    //console.log("response: %s", http_request.responseText);
    if (http_request.readyState == 4) {
        // JavaScript doesn't do integer division
        if (http_request.status - (http_request.status % 100) != 200) {
            // We included the Accept:application/json above: the responseText should be JSON
            var errorEvent = eval("("+http_request.responseText+")");
            alert("There was a problem with the request: " + errorEvent.reason
                  + ": " + errorEvent.description
                  + " (url=" +url+")");
        } else {
		alert("Get has been successful");
          
        }
    }
    return 0;
}

