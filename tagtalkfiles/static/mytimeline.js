        google.load("visualization", "1");

        // Set callback to run when API is loaded
        google.setOnLoadCallback(drawVisualization);

        var timeline;
        var data;
	//Global list of all the tags, filled from a get response from the server
	//It is in the format Time of tag, *no end time* , tag name, tag description, tag post 
	var tags = [[new Date(0,0,0,0), , , 'DEFAULT TAG']];
	

	//get all of the tags to be included
	function getTags(){
	//will come from a get but at the moment from a hard coded list
		tags.push([new Date(2013,0,5,11,20), ,'Tag 2.', 'Tag 2 Description']);
		tags.push([new Date(2013,0,5,11,10), ,'Tag 1.', 'Tag 1 Description']);
	}

        // Called when the timelineualization API is loaded.
        function drawVisualization() {
            // Create and populate a data table.
            data = new google.visualization.DataTable();
            data.addColumn('datetime', 'start');
            data.addColumn('datetime', 'end');
            data.addColumn('string', 'content');
	    data.addColumn('string', 'description');

            function addRow(startDate, endDate, content, backgroundColor, borderColor)
            {
                // we make our own customized layout for the events

                if (backgroundColor == undefined)
                    backgroundColor = "yellow";
                if (borderColor == undefined)
                    borderColor = "gold";

                var fill = endDate ? "pink" : "yellow";
                var div = '<div style="background-color:' + backgroundColor +
                        '; border:1px solid ' + borderColor + ';padding:5px;">' +
                        content + '</div>';

                data.addRows([
                    [startDate, endDate, div]
                ]);
            }
	     
	    getTags();

            data.addRows(tags);
	    
	    document.getElementById('container').value = data.getNumberOfColumns();

            // specify options
            var options = {
                width:     "90%",
                height:    "auto",
                start: new Date(2013,0,5,11,0),
                end:   new Date(2013,0,5,12,0),
                style:    "box"    // optional. choose "dot" (default), "box", or "none"
            };

            // Instantiate our table object.
            timeline = new links.Timeline(document.getElementById('mytimeline'));

            // Attach event listeners
            google.visualization.events.addListener(timeline, 'select', onselect);
            google.visualization.events.addListener(timeline, 'rangechange', onrangechange);

            // Draw our table with the data we created locally.
            timeline.draw(data, options);

            // Set the scale by hand. Autoscaling will be disabled.
            // Note: you can achieve the same by specifying scale and step in the
            // options for the timeline.
            //timeline.setScale(links.Timeline.StepDate.SCALE.DAY, 1);
        }

        // adjust start and end time.
        function setTime() {
            if (!timeline) return;

            var newStartDate = new Date(document.getElementById('startDate').value);
            var newEndDate   = new Date(document.getElementById('endDate').value);
            timeline.setVisibleChartRange(newStartDate, newEndDate);
            timeline.redraw();
        }

        function onrangechange() {
            // adjust the values of startDate and endDate
            var range = timeline.getVisibleChartRange();
            document.getElementById('startDate').value = dateFormat(range.start);
            document.getElementById('endDate').value   = dateFormat(range.end);
        }
	
	

        function onselect() {
            var sel = timeline.getSelection();
            if (sel.length) {
                if (sel[0].row != undefined) {
                    var row = sel[0].row;
                   	HelpBalloon.Options.prototype = Object.extend(HelpBalloon.Options.prototype, {
			icon: 'helpballoon/images/icon.gif',
			button: 'helpballoon/images/button.png',
			balloonPrefix: 'helpballoon/images/balloon-'
			});
			hb = new HelpBalloon({
				title: 'Tag Selected: XXXXX ',
				content: 'Tag Desription: XXXXX',		
				icon: $('tagInfo'),
				anchorPosition: 'centre right'});
		   
		    hb.show();
                }
            }
        }

        // Format given date as "yyyy-mm-dd hh:ii:ss"
        // @param datetime   A Date object.
        function dateFormat(date) {
            datetime =   date.getFullYear() + "-" +
                    ((date.getMonth()   <  9) ? "0" : "") + (date.getMonth() + 1) + "-" +
                    ((date.getDate()    < 10) ? "0" : "") +  date.getDate() + " " +
                    ((date.getHours()   < 10) ? "0" : "") +  date.getHours() + ":" +
                    ((date.getMinutes() < 10) ? "0" : "") +  date.getMinutes() + ":" +
                    ((date.getSeconds() < 10) ? "0" : "") +  date.getSeconds()
            return datetime;
        }
