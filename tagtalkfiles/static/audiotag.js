var tl;                         // the timeline
var allEventSource;             // the source of all events
var currentRecording = null;    // notional current recording
var allRecordings = {};         // a hash of 'url' -> recording-event
var mainBand;                   // the index of the 'principal' band on the timeline



// The HTML which calls this should set audiotagBase to the path which
// "static/" is relative to, for example "/" or "/podcasting/".

// The following is an attempt at a cross-browser logging function,
// but it doesn't seem to work _anywhere_!
// var logMessage;
// if (typeof console !== 'undefined') {
//     logMessage = function(aMessage) {
//         console.info("audiotag: " + aMessage);
//     }
// } else {
//     // Chrome version?
//     logMessage = function(aMessage) {
//         dump("audiotag^2: " + aMessage);
//     }
// }


// Parameters:
//   dataUrl: a URL from which the function can read timeline data.
//   startAtRecordingUrl: a URL indicating which URL, of the list of
//       recording URLs available within the data source, is first to
//       be taken as the currentRecording.
//   longstyle_p: a binary flag, indicating which of two slightly different
//       display formats we should use.
function onLoad(dataUrl, startAtRecordingUrl, longstyle_p, current_user_p) {        google.load("visualization", "1");

        // Set callback to run when API is loaded
        google.setOnLoadCallback(drawVisualization);

        var timeline;
        var data;

        // Called when the timelineualization API is loaded.
        function drawVisualization() {
            // Create and populate a data table.
            data = new google.visualization.DataTable();
            data.addColumn('datetime', 'start');
            data.addColumn('datetime', 'end');
            data.addColumn('string', 'content');

            function addRow(startDate, endDate, content, backgroundColor, borderColor)
            {
                // we make our own customized layout for the events

                if (backgroundColor == undefined)
                    backgroundColor = "yellow";
                if (borderColor == undefined)
                    borderColor = "gold";

                var fill = endDate ? "pink" : "yellow";
                var div = '<div style="background-color:' + backgroundColor +
                        '; border:1px solid ' + borderColor + ';padding:5px;">' +
                        content + '</div>';

                data.addRows([
                    [startDate, endDate, div]
                ]);
            }

            data.addRows([
                [new Date(2013,0,5,11,10), ,'Tag 1.'],
		[new Date(2013,0,5,11,20), ,'Tag 2.'],
		[new Date(2013,0,5,11,30), ,'Tag 3.'],
		[new Date(2013,0,5,11,40), ,'Tag 4.'],
		[new Date(2013,0,5,11,50), ,'Tag 5.'],
		[new Date(2013,0,5,11,15), ,'Tag 11.'],
		[new Date(2013,0,5,11,25), ,'Tag 12.'],
		[new Date(2013,0,5,11,35), ,'Tag 13.'],
		[new Date(2013,0,5,11,45), ,'Tag 14.'],
		[new Date(2013,0,5,11,55), ,'Tag 15.'],
		[new Date(2013,0,5,11,12), ,'Tag 21.'],
		[new Date(2013,0,5,11,22), ,'Tag 22.'],
		[new Date(2013,0,5,11,34), ,'Tag 23.'],
		[new Date(2013,0,5,11,43), ,'Tag 24.'],
		[new Date(2013,0,5,11,52), ,'Tag 25.'],
		[new Date(2013,0,5,11,11), ,'Tag 6.'],
		[new Date(2013,0,5,11,22), ,'Tag 7.'],
		[new Date(2013,0,5,11,33), ,'Tag 8.'],
		[new Date(2013,0,5,11,44), ,'Tag 9.'],
		[new Date(2013,0,5,11,57), ,'Tag 10.'],
		[new Date(2013,0,5,11,13), ,'Tag 16.'],
		[new Date(2013,0,5,11,23), ,'Tag 17.'],
		[new Date(2013,0,5,11,34), ,'Tag 18.'],
		[new Date(2013,0,5,11,46), ,'Tag 19.'],
		[new Date(2013,0,5,11,53), ,'Tag 20.']
            ]);

            // specify options
            var options = {
                width:     "90%",
                height:    "auto",
                start: new Date(2013,0,5,11,0),
                end:   new Date(2013,0,5,12,0),
                style:    "box"    // optional. choose "dot" (default), "box", or "none"
            };

            // Instantiate our table object.
            timeline = new links.Timeline(document.getElementById('mytimeline'));

            // Attach event listeners
            google.visualization.events.addListener(timeline, 'select', onselect);
            google.visualization.events.addListener(timeline, 'rangechange', onrangechange);

            // Draw our table with the data we created locally.
            timeline.draw(data, options);

            // Set the scale by hand. Autoscaling will be disabled.
            // Note: you can achieve the same by specifying scale and step in the
            // options for the timeline.
            //timeline.setScale(links.Timeline.StepDate.SCALE.DAY, 1);
        }

        // adjust start and end time.
        function setTime() {
            if (!timeline) return;

            var newStartDate = new Date(document.getElementById('startDate').value);
            var newEndDate   = new Date(document.getElementById('endDate').value);
            timeline.setVisibleChartRange(newStartDate, newEndDate);
            timeline.redraw();
        }

        function onrangechange() {
            // adjust the values of startDate and endDate
            var range = timeline.getVisibleChartRange();
            document.getElementById('startDate').value = dateFormat(range.start);
            document.getElementById('endDate').value   = dateFormat(range.end);
        }
	
	

        function onselect() {
            var sel = timeline.getSelection();
            if (sel.length) {
                if (sel[0].row != undefined) {
                    var row = sel[0].row;
                       hb = new HelpBalloon({
			title: '<Tag Clicked>',
			content: '<Tag Desription >\n' + 'is is an example of static '
			+ 'balloon content.',
			icon: $('tagInfo'),
			anchorPosition: 'centre right'
			});
		    hb.show();
                }
            }
        }

        // Format given date as "yyyy-mm-dd hh:ii:ss"
        // @param datetime   A Date object.
        function dateFormat(date) {
            datetime =   date.getFullYear() + "-" +
                    ((date.getMonth()   <  9) ? "0" : "") + (date.getMonth() + 1) + "-" +
                    ((date.getDate()    < 10) ? "0" : "") +  date.getDate() + " " +
                    ((date.getHours()   < 10) ? "0" : "") +  date.getHours() + ":" +
                    ((date.getMinutes() < 10) ? "0" : "") +  date.getMinutes() + ":" +
                    ((date.getSeconds() < 10) ? "0" : "") +  date.getSeconds()
            return datetime;
        }

    // Replace the original _onClickInstantEvent handler, in the prototype
    Timeline.OriginalEventPainter.prototype._onClickInstantEvent = function(icon, domEvt, evt) {
        setCurrentEvent(evt);
    }
    if (! longstyle_p) {
        // keep track of the 'current event' only in the non-longstyle layout
        Timeline.OriginalEventPainter.prototype._onClickDurationEvent = function(icon, domEvt, evt) {
            setCurrentEvent(evt);
        }
    }
    tl.getBand(mainBand).addOnScrollListener(function(band) {
        if (currentEvent !== undefined) {
            // Clear the current event whenever we scroll the band
            setCurrentEvent(undefined);
        }
    });

    if (current_user_p) {
        var tagbox = document.getElementById("tagbox");
        // Add the handleTagbox function as a listener for keypresses in the tagbox.
        if (tagbox.addEventListener) {
            tagbox.addEventListener("keypress",
                                    { // implement EventListener interface
                                        // (to be honest, just passing handleTagbox here works)
                                        handleEvent: handleTagbox },
                                   false);
        } else {
            // This (I think) is a workaround for old IE versions.
            // I've no idea if this is really necessary, or indeed
            // whether it works.
            tagbox.attachEvent("onkeypress", handleTagbox);
        }
    }

   // initialize the visual state of the play button
    updatePlayButtonState();
    // set the skipIndex to default 1, and fill in the corresponding display box
    setSkipAmount(+1);
}

////////////////////////////////////////
//
// The 'current event'

// Maintain a 'current event', which is the event which is displayed
// in the event display box at the bottom of the timeline page, and
// which is (well, should be) highlighted.
var currentEvent;

// Function to be passed to setHightlightMatcher
// Returns highlight 0 if this is the current tag, and highlight 1 if
// it's (a non-current) one of the user's own tags.  Return -1 -- no
// highlighting -- if neither is true.
function currentEventHighlighter(evt) {
    var code = 0;               // integer highlight style
    if (currentEvent && currentEvent.getID && (evt.getID() == currentEvent.getID())) {
        code = code + 1; // selected event
    }
    if (evt.getProperty("aut_ownerp")) {
        code = code + 2; // my event
    }
    return (code > 0 ? code : -1);
}

// This array describes the fields which are displayed in the 'current event' box.
// name: the element name
// mustBeOwner: the current user must be the owner of this event, to
//     be allowed to change it
// formElement: what type of form element should be presented to
//     change this (default is 'text') 
// displayLegend: the label beside the current value (default is the
//     value of the 'name' attribute) 
// makeContent: a function which returns an element to be included in
//     the 'values' column of the resulting table.  If not present,
//     then we simply use the corresponding value of the event.
// processValue: if present, this handed the value present in the form
//     at submission; it does any processing required, and returns a
//     string of the form '(key ...)', which is to be included in the
//     submitted sexp.  If the value is invalid, then produce an alert and
//     return false.
var currentEventEditInfo = [/*
                              { name: "title",
                              mustBeOwner: true },
                            { name: "description",
                              formElement: "textarea",
                              displayLegend: "tagger's notes",
                              mustBeOwner: true },
                            { name: "comments",
                              formElement: "textarea",
                              makeContent: function(ev) {
                                  return makeElement("div",
                                                     {},
                                                     (ev.getProperty("comments") || []).map(function(c) {
                                                         return makeElement("p",
                                                                            { _class: (c.aut_ownerp ? "text-myselected" : "text-selected") },
                                                                            [ c.comment + " ",
                                                                              makeElement("span",
                                                                                          { _class: "discreet" },
                                                                                          [ c.ownername+", "+c.date ]) ]);
                                                     }));
                              },
                              mustBeOwner: false }, */
                            { name: "tags",
                              makeContent: function(ev) {
                                  var span = document.createElement("span");
                                  (ev.getProperty("tags") || []).forEach(function(tag) {
                                      var tagspan = makeElement("span",
                                                                { _class: "tag " + (tag.aut_ownerp ? "text-myselected" : "text-selected") },
                                                                [ makeElement("a", { href: audiotagBase+"tag/"+tag.tag }, [ tag.tag ])]);
                                      if (tag.aut_ownerp) {
                                          tagspan.appendChild(makeElement("button",
                                                                          { type: "button",
                                                                            onclick: "deleteOneTag(\""+ev.getProperty("url")+"/"+tag.tag+"\")" },
                                                                          [makeElement("img",
                                                                                       { src: audiotagBase+"static/delete4.png",
                                                                                         alt: "delete tag",
                                                                                         title: "delete tag" },
                                                                                       [])]));
                                      } else {
                                          tagspan.appendChild(makeElement("span",
                                                                          { _class: "inconspicuous" },
                                                                          [ tag.owner ]));
                                      }
                                      span.appendChild(tagspan);
                                      span.appendChild(document.createTextNode(" "));
                                  });
                                  return span;
                              },
                              processValue: function(value) {
                                  if (isValidTag(value)) {
                                      return "(tags \"" + value + "\")";
                                  } else {
                                      alert("The string "+value+" isn't a suitable tag");
                                      return false;
                                  }
                              },
                              mustBeOwner: false
                            }
                            /*
                            { name: "extent",
                              mustBeOwner: true,
                              formElement: "select||instant|duration, with endpoint here",
                              makeContent: function(ev) {
                                  return makeElement("p",
                                                     { _class: (ev.getProperty("aut_ownerp") ? "text-myselected" : "text-selected") },
                                                     [ (ev.isInstant() ? "instant" : "duration, "+ convertMsToMins(ev.getEnd()-ev.getStart()) ) ]);
                              },
                              processValue: function(value) {
                                  var band = tl.getBand(mainBand);
                                  var currTime = band.getCenterVisibleDate();
                                  if (currentEvent.getProperty("aut_isrecording")) {
                                      alert("That's a recording: You can't change a recording to an instant!");
                                      return false;
                                  } else if (value === "") {
                                      return "";
                                  } else if (value === "instant") {
                                      if (currentEvent.isInstant()) {
                                          return "";
                                      } else {
                                          if (currTime < currentEvent.getStart()) {
                                              band.setCenterVisibleDate(currentEvent.getStart());
                                              return "(date \"" + makeAudiotagTimespec(currentEvent.getEnd()) + "\")";
                                          } else {
                                              band.setCenterVisibleDate(currentEvent.getEnd());
                                              return "(date \"" + makeAudiotagTimespec(currentEvent.getStart()) + "\")";
                                          }
                                      }
                                  } else {
                                      // value is "duration" Whether or not this is an instant or a
                                      // duration event, set it to be a duration event with the
                                      // current time as its start/endpoint
                                      if (currTime < currentEvent.getStart()) {
                                          return "(date \"" + makeAudiotagTimespec(currTime)
                                              + "\" \"" + makeAudiotagTimespec(currentEvent.getStart()) + "\")";
                                      } else {
                                          return "(date \"" + makeAudiotagTimespec(currentEvent.getStart())
                                              + "\" \"" + makeAudiotagTimespec(currTime) + "\")";
                                      }
                                  }
                              }
                              }*/
];
var updateButtonTD;

// makeElement : string hash list -> HTMLElement
// Construct an HTML element with attributes taken from ATTS, and
// the specified CHILDREN.  If one of the att names starts with an
// underscore, then this is removed before being used (this means you
// can specify  { _class: "foo" }, and not cause a stupid Javascript error).
function makeElement(name, atts, children) {
    var el = document.createElement(name);
    for (var att in atts) {
        if (att.charAt(0) === '_') {
            el.setAttribute(att.substring(1), atts[att]);
        } else {
            el.setAttribute(att, atts[att]);
        }
    }
    children.forEach(function(c) {
        if (typeof(c) == "string") {
            el.appendChild(document.createTextNode(c));
        } else {
            el.appendChild(c);
        }
    });
    return el;
}

// Called when a new 'current event' is chosen.  This sets the
// currentEvent object, and also updates the event display box.
//
// There's something messed up with the layout.  If I call
// tl.layout(), or if I resize the window, then the timeline is
// shifted to some apparently random other point.
function setCurrentEvent(e) {
    var currentEventBox = document.getElementById("currentevent");
    if (e === undefined) {
        // we're unsetting the current event
        while (currentEventBox.lastChild != null) {
            currentEventBox.removeChild(currentEventBox.lastChild);
        }
    } else if (currentEvent === undefined) {
        // first time here -- create the current event display box
        while (currentEventBox.lastChild != null) {
            currentEventBox.removeChild(currentEventBox.lastChild);
        }
        var form = makeElement("form", {id: "currentEventUpdateForm"}, []);
        currentEventBox.appendChild(form);

        currentEventEditInfo.forEach(function(info, idx, parent) {
            var para = document.createElement("p");
            para.appendChild(document.createTextNode((info.displayLegend || info.name) + ": "));
            var content = document.createElement("span");
            para.appendChild(content);
            info["displayElement"] = content;
            form.appendChild(para);
        });
        currentEventBox.style.display = '';
    }
    currentEvent = e;
    if (currentEventBox) {
        updateCurrentEventDisplay();
    }
    tl.paint();
}
// The following is an alternative implementation of setCurrentEvent,
// which has a much richer interface, controlled by
// currentEventEditInfo.  This is probably unnecessarily complicated
// for this version of the UI, but keep it here in case we need to
// develop this further.
function setCurrentEventTABULAR(e) {
    var currentEventBox = document.getElementById("currentevent");
    if (currentEvent === undefined) {
        // first time here -- create the current event display box
        while (currentEventBox.lastChild != null) {
            currentEventBox.removeChild(currentEventBox.lastChild);
        }
        var form = makeElement("form", {id: "currentEventUpdateForm"}, []);
        currentEventBox.appendChild(form);
        var table = document.createElement("table");
        form.appendChild(table);
        var tr;
        var td;
        var info;

        currentEventEditInfo.forEach(function(info, idx, parent) {
            tr = table.insertRow(idx);
            td = tr.insertCell(0);
            td.setAttribute("class", "left");
            td.innerHTML = (info.displayLegend || info.name);
            td = tr.insertCell(1);
            td.innerHTML = "x";
            info["displayElement"] = td;
            td = tr.insertCell(2);
            if (info.formElement == "textarea") {
                td.appendChild(makeElement("textarea",
                                           { name: info.name,
                                             rows: 3,
                                             cols: 40 },
                                           []));
            } else if (/^select/.test(info.formElement)) {
                var options = info.formElement.split("|");
                options.shift();
                var select = makeElement("select",
                                         { name: info.name },
                                         options.map(function(opt) { return makeElement("option", {}, [ opt ]); }));
                select.firstChild.setAttribute("selected", "selected");
                td.appendChild(select);
            } else {
                td.appendChild(makeElement("input",
                                           { name: info.name, type: "text", value: "" },
                                           []));
            }
            info["editElement"] = td;
            td.style.display = 'none';
        });
        if (document.getElementById("newtaginput")) {
            // We don't do anything with this element, but its
            // presence indicates that the user is logged in and
            // allowed to post tags.
            tr = table.insertRow(currentEventEditInfo.length);
            tr.setAttribute("style", "border-bottom: none");
            td = tr.insertCell(0);
            td = tr.insertCell(1);
            td.appendChild(makeElement("button",
                                       { type: "button",
                                         onclick: "showUpdateCurrentEvent(true)" },
                                       [ makeElement("img",
                                                     { src: audiotagBase + "static/edit3.png",
                                                       alt: "Edit this",
                                                       title: "Edit" }, [])]));
            td = tr.insertCell(2);
            td.appendChild(makeElement("button",
                                       { type: "button",
                                         onclick: "currentEventUpdateFormSubmit()" },
                                       [ makeElement("img",
                                                     { src: audiotagBase + "static/plus3.png",
                                                       alt: "Update this event",
                                                       title: "Update this event" }, [])]));
            td.appendChild(document.createTextNode("  "));
            td.appendChild(makeElement("button",
                                       { type: "button",
                                         onclick: "deleteOneEvent(\""+e.getProperty("url")+"\")" },
                                       [ makeElement("img",
                                                     { src: audiotagBase + "static/delete3.png",
                                                       alt: "delete event",
                                                       title: "Delete this event" }, [])]));
            td.appendChild(document.createTextNode("  "));
            td.appendChild(makeElement("button",
                                       { type: "button",
                                         onclick: "showUpdateCurrentEvent(false)" },
                                       [ document.createTextNode("Cancel") ]));
            td.style.display = 'none';
            updateButtonTD = td;
        }
        currentEventBox.style.display = '';
    }
    currentEvent = e;
    if (currentEventBox) {
        updateCurrentEventDisplay();
    }
    tl.paint();
}

// Enables or disables the display of the editing column of the current element box.
// If the user is not the owner of the current event, then they can
// only edit the event if its .mustBeOwner property is false.
function showUpdateCurrentEvent(showIt) {
    var owner = (currentEvent && currentEvent.getProperty("aut_ownerp"));
    if (showIt) {
        currentEventEditInfo.forEach(function(info) {
            if (owner || ! info.mustBeOwner) {
                info["editElement"].style.display = '';
            }
        });
        updateButtonTD.style.display = '';
    } else {
        currentEventEditInfo.forEach(function(info) {
            info["editElement"].style.display = 'none';
        });
        updateButtonTD.style.display = 'none';
    }
}

function updateCurrentEventDisplay() {
    if (currentEvent !== undefined) {
        currentEventEditInfo.forEach(function(info) {
            var el = info.displayElement;
            if (info.makeContent) {
                while (el.lastChild != null) {
                    el.removeChild(el.lastChild);
                }
                el.appendChild(info.makeContent(currentEvent));
            } else {
                el.innerHTML = (currentEvent.getProperty(info.name) || "");
                el.setAttribute("class",
                                currentEvent.getProperty("aut_ownerp") ? "text-myselected" : "text-selected");
            }
        });
    }
}

function currentEventUpdateFormSubmit() {
    var form = document.getElementById("currentEventUpdateForm");
    var postText = "(event (id \"" + currentEvent.getID() + "\")";
    var postit = true;          // no errors
    myforEach(function(element) { // ordinary Array.forEach appears not to work here!
        if (element.type != "button" && element.value != "") {
            var updateAtts;
            // find the element in currentEventEditInfo[] which corresponds to this element
            for (var ai=0; ai<currentEventEditInfo.length; ai++) {
                if (currentEventEditInfo[ai].name == element.name) {
                    updateAtts = currentEventEditInfo[ai];
                    break;      // JUMP OUT
                }
            }                   // ...to here
            if (updateAtts && updateAtts.processValue) {
                var goodValue = updateAtts.processValue(element.value);
                if (goodValue) {
                    postText = postText + " " + goodValue;
                } else {
                    postit = false; // .processValue has produced an alert
                }
            } else {
                postText = postText + " (" + element.name
                    + " \"" + escapeQuotes(element.value)
                    + "\")";
            }
        }
    }, form.elements);
    if (postit) {
        postText = postText + ")";
        var postUrl = currentRecording.aut_recordingUrl;
        makeAndHandlePOST(postUrl, postText, false);
        showUpdateCurrentEvent(false); // hide the edit-tag box

        // We want the current event box to reflect the change, so
        // clear the allEventSource (which triggers a reload), and
        // call the function to update that display.
        // XXX I don't think this fully works, yet (not sure why).
        allEventSource.clear();
        updateCurrentEventDisplay();
    }
}

// Display the current time
var timebox;
var timeboxlink;
// Show the time in the box under the timeline.
// Additionally, show a link to 'now' in the <a href='#nnnms'> element.
function showTime(band)
{
    if (!timebox) {
        timebox = document.getElementById("timeboxtime");
        timeboxlink = document.getElementById("timeboxlink").attributes.getNamedItem("href");
    }
    if (timebox && currentRecording) {
        var currentTimelineTime = band.getCenterVisibleDate().getTime();
        // getTime returns ms
        var offset_ms = (currentTimelineTime - currentRecording.start);
        timeboxlink.textContent = "#" + offset_ms + "ms";
        var offset_s = Math.round(offset_ms / 1000);
        var secs = offset_s % 60;
        var mins = (offset_s - secs) / 60;
          if (offset_s == 0) {
            timebox.innerHTML = "start";
        } else if (secs < 10) {
            timebox.innerHTML = "" + mins + ":0" + secs;
        } else {
            timebox.innerHTML = "" + mins + ":" + secs;
        }
    }
}

// Zoom in or out (depending on whether zoomInp is true or false) the indicated band.
function zoomIn(bandNo, zoomInp) {
    tl.getBand(bandNo).zoom(zoomInp);
    tl.getBand(bandNo).paint();
}

////////////////////////////////////////
//
// Sound

// Hold on to the current sound
var audiotagSound = null;

/* 
 * updatePlayButtonState :  -> void
 * Show either the pause or the play button, depending on whether the
 * sound is or is not playing, respectively.
 * If the argument is positive, then show the play button, if non-positive, show the pause button.
 */
function updatePlayButtonState()
{
    var playButton = document.getElementById("play-button");
    var pauseButton = document.getElementById("pause-button");
    if (playButton && pauseButton) {
        // we are indeed on a timeline page which has play/pause buttons
        // (that's reassuring...)
        if (audiotagSound === null || audiotagSound.playState == 0) {
            // not playing -- show the play button
            playButton.style.display  = '';
            pauseButton.style.display = 'none';
        } else {
            // playing -- show the pause button
            playButton.style.display  = 'none';
            pauseButton.style.display = '';
        }
    }
}

function startPlaying()
{
    var bandToUpdate = tl.getBand(mainBand);
    var currTime = bandToUpdate.getCenterVisibleDate();
    var rec = findRecordingOverlappingTime(currTime);
    var offset;
    if (rec === null) {
        // There was no recording overlapping the cursor, so start
        // playing the currentRecording from the beginning.
        rec = currentRecording;
        offset = 0;
    } else {
        // There is a recording overlapping the cursor.  So ensure
        // that this is the current recording, and proceed to play
        // this recording, starting from our current position,
        // according to the timeline.
        currentRecording = rec;
        offset = currTime.getTime() - currentRecording.start.getTime();
    }

    if (rec === null) {
        // currentRecording appears to be null
        // (that's a little odd, but don't collapse)
        alert("There appears to be no current recording");
    } else {
        var eventStartTime = rec.start.getTime();
        if (audiotagSound === null) {
            var recordingURL = rec.aut_recordingUrl;
            var audioURL = rec.aut_audioUrl;
            audiotagSound = soundManager.createSound({ id: recordingURL, // use URL as ID, easing later retrieval
                                                       url: audioURL });
        }
        if (audiotagSound === null) { // still...
            alert("Odd: I'm having trouble creating that sound (from " + audioURL + ")");
        } else {
            audiotagSound.play({
                onload: function() {
                    audiotagSound._writeDebug("Sound " + this.id + " at position "
                                + this.position + "/"
                                + this.duration + " is loaded");
                },
                whileplaying: function() {
                    // wait until here to update the position,
                    // since setPosition won't work until after the audio is loaded
                    if (offset !== null) {
                        audiotagSound.setPosition(offset);
                        offset = null;
                    }
                    bandToUpdate.setCenterVisibleDate(eventStartTime + audiotagSound.position);
                },
                onfinish: updatePlayButtonState
            });
            updatePlayButtonState();
        }
    }
}

function stopPlaying()
{
    if (audiotagSound !== null)
    {
        audiotagSound.stop();
        updatePlayButtonState();
    }
}

// An array of skip sizes, in seconds
var skipAmounts = [ 5, 10, 15, 30, 60, 120, 300 ];
var skipIndex = 0;
function setSkipAmount(direction) 
{
    if (direction > 0) {
        if (skipIndex < skipAmounts.length-1)
            skipIndex = skipIndex + 1;
    } else {
        if (skipIndex > 0)
            skipIndex = skipIndex - 1;
    }
    var skipsize = document.getElementById("skipsize");
    if (skipsize) {
        // don't attempt to set this, if there isn't an element with ID "skipsize" (presumably the
        // page we're on has no need for this, but this function was still called during
        // initialization, in onLoad().
        skipsize.innerHTML = skipAmounts[skipIndex];
    }
}

// Move the cursor forward or backward into the recording,
// depending on the sign of the 'nudge' parameter.
// The size of the nudge parameter is ignored.
function skipPosition(nudge)
{
    if (audiotagSound !== null) {
        var nudgeAmount = skipAmounts[skipIndex] * 1000;
        if (nudge < 0) {
            nudgeAmount = -nudgeAmount;
        }
        audiotagSound.setPosition(audiotagSound.position + nudgeAmount);
    }
}

// Set the position in the recording.
// If fromStart is true, offset is ms from the start;
// if it's false, offset is ms back from the end.
// XXX This seems occasionally to slighly overshoot at one end or the other (by ms),
// and leave the cursor after the end of the recording.
function setPosition(offset, fromStart)
{
    if (audiotagSound !== null) {
        stopPlaying();
    }
    // no sound is playing
    if (fromStart) {
        tl.getBand(mainBand).setCenterVisibleDate(currentRecording.start.getTime() + offset);
    } else {
        tl.getBand(mainBand).setCenterVisibleDate(currentRecording.end.getTime() - offset);
    }
    showTime(mainBand);
}

////////////////////////////////////////
//
// Posting tags and other edits

// Extract a character, in a string, from a keypress event
// (see http://javascript.info/tutorial/keyboard-events)
function getChar(event) {
    // event.type must be keypress
    if (event.which == null) {
	return String.fromCharCode(event.keyCode); // IE
    } else if (event.which!=0 && event.charCode!=0) {
	return String.fromCharCode(event.which);   // the rest
    } else {
	return null; // special key
    }
}

var tagboxValidTagChars = /[A-Za-z0-9._-]/;
var tagboxWhitespace = /\s/;
// Handler for keys entered into tagbox
function handleTagbox(e) {
    var insertThisCharacter = true;
    var tagbox = document.getElementById("tagbox");
    if (tagbox.className == "defaultValue") {
        tagbox.value = '';
        tagbox.className = '';
    }
    var c = getChar(e);
    if (tagboxWhitespace.test(c)) {
        // whitespace, including the return/EOL which submits the tags
        if (tagbox.value != "") {
            // The postNewTag method relies on the format of this <p> element
            var newbox = makeElement("span", { _class: "tag text-mine" }, [ tagbox.value ]);
            // Add this newbox to the beginning of the list which
            // contains the current element.  Deliberately add them to
            // the beginning of the list: this makes them appear in
            // the 'wrong' order, and so should dissuade people from
            // thinking they're adding a multi-word tag.
            tagbox.parentNode.insertBefore(newbox, tagbox.parentNode.childNodes[0]);
            tagbox.value = "";
        }
        if (c == " ") {
            e.preventDefault();
            insertThisCharacter = false;
        }
        // but let the return/EOL characters through
    } else {
        if (! tagboxValidTagChars.test(c)) {
            alert("Bad character for tag: " + c);            
            e.preventDefault();
            insertThisCharacter = false;
        }
    }
    return insertThisCharacter;
}

// Work through the recordings in the allRecordings hash, to find one
// which overlaps with the given time.
function findRecordingOverlappingTime(time)
{
    for (var recname in allRecordings) {
        var rec = allRecordings[recname];
        if (Timeline.NativeDateUnit.compare(rec.start, time) <= 0
            && Timeline.NativeDateUnit.compare(time, rec.end) <= 0) {
            return rec;
        }
    }
    return null;
}

function isValidTag(tag) {
    var validTag = /^[A-Za-z0-9._-]+$/;
    return validTag.test(tag);
}

function postNewTag(collectionName)
{
    var alltags = new Array();
    var tagbox = document.getElementById("tagbox");
    // <form><p><span class='tag xxx'>foo</span>...<input type='text' id='tagbox'/></p></form>
    var para = tagbox.parentNode;
    // Work through the list of child nodes in the following way,
    // rather than the more obvious for loop: calling removeChild
    // changes the length of the list, which causes the for loop to
    // exit early.
    while (para.childNodes.length > 0) {
        var n = para.childNodes[0];
        if (n.getAttribute("class").search(/tag/) >= 0) {
            alltags.push(n.textContent.replace(/\s+/,""));
            para.removeChild(n);
        } else {
            // we've seen all of the span elements, so stop
            // (this depends on the form of the <p> element contents)
            break;
        }
    }
    if (tagbox.value.search(/\S/) >= 0) {
        alltags.push(tagbox.value.replace(/\s+/,""));
    }
    if (alltags.length == 0) {
        // no tags added
        return false;
    }
    alltags.forEach(function(t) {
        // this check should be redundant, since the tagbox handler
        // should prevent any invalid tags being created
        if (! isValidTag(t)) {
            alert("Tags should consist of only letters, digits, dots, dashes and underscores");
            return false;
        }});

    var request;
    var postingurl;

    // If there is a current event, then add these tags to that; if
    // not, create a new event at the current time.
    if (currentEvent !== undefined) {
        request = '(event (id "' + currentEvent.getID() + '") (tags "' + alltags.join('" "') + '"))';
        postingurl = currentRecording.aut_recordingUrl;
    } else {
        // get the current time
        var currTime = tl.getBand(mainBand).getCenterVisibleDate()
        var timespec = makeAudiotagTimespec(currTime);
        var coveringRecording = findRecordingOverlappingTime(currTime);
        if (coveringRecording == null) {
            alert("There is no audio here to tag");
            return false;
        }
        postingurl = coveringRecording.aut_recordingUrl;
        // assemble the text to be posted to the server
        request = '(event (date "' + timespec + '") (tags "' + alltags.join("\" \"") + '"))';
    }

    //alert("Posting... " + request);
    makeAndHandlePOST(postingurl, request, true);

    // return the tagbox to its 'inviting' state
    tagbox.value = 'tag...';
    tagbox.className = 'defaultValue';

    // this is called as an onsubmit action: suppress the browser's submit action
    return false;
}

function makeAndHandlePOST(url, payload, redisplay)
{
    var http_request = SimileAjax.XmlHttp._createRequest();
    if (!http_request) {
        alert('Giving up: Cannot create an XMLHTTP instance');
        return false;
    }
    http_request.onreadystatechange = function() {
        handlePostResponse(http_request, url, redisplay);
    };
    http_request.open('POST', url, true);
    http_request.setRequestHeader("Content-Type", "text/plain");
    http_request.setRequestHeader("Accept", "application/json");
    //console.log("Request: " + payload);
    http_request.send(payload);
}

function handlePostResponse(http_request, url, reload)
{
    //console.log("response: %s", http_request.responseText);
    if (http_request.readyState == 4) {
        // JavaScript doesn't do integer division
        if (http_request.status - (http_request.status % 100) != 200) {
            // We included the Accept:application/json above: the responseText should be JSON
            var errorEvent = eval("("+http_request.responseText+")");
            alert("There was a problem with the request: " + errorEvent.reason
                  + ": " + errorEvent.description
                  + " (url=" +url+")");
        } else {
            if (reload) {
                // reload is necessary if we've added a new event
                var newBareEvent = eval("("+http_request.responseText+")");
                // FIXME: if we've added another tag to an event, this
                // results in the tag appearing twice in the timeline,
                // once with and once without the new tag; perhaps
                // work out how to remove the now-duplicate element of
                // the allEventSource array (the newBareEvent.id
                // should let you find it)
                allEventSource.loadJSON(newBareEvent, url);
            }
        }
    }
    return 0;
}

function deleteUrl(url) {
    var http_request = SimileAjax.XmlHttp._createRequest();
    if (!http_request) {
        alert('Giving up: Cannot create an XMLHTTP instance');
        return false;
    }
    http_request.onreadystatechange = function() {
        if (http_request.readyState == 4) {
            alert("Deleted!");
        }
    };
    http_request.open('DELETE', url, true);
    http_request.send();
}


function deleteOneTag(tagurl) {
    deleteUrl(tagurl);
}
function deleteOneEvent(eventurl) {
    var do_it = confirm("Delete event " + eventurl);
    if (do_it) {
        deleteUrl(eventurl);
    }
}

////////////////////////////////////////
//
// Useful functions

// functional usefulnesses
function myforEach(func, arr) {
    for (var i=0; i<arr.length; i++) {
        func(arr[i]);
    }
}

// define a suitable onResize() method, which calls tl.layout()
var resizeTimerID = null;
function onResize() {
    if (resizeTimerID == null) {
        resizeTimerID = window.setTimeout(function() {
            resizeTimerID = null;
            tl.layout();
        }, 500);
    }
}

// Remove the 'defaultValue' class from the given object
function unDefault(id) {
    var el = document.getElementById(id);
    if (el && el.className == 'defaultValue') {
        el.value = '';
        el.className = '';
    }
}

// return a string with quotes escaped with '\"'
function escapeQuotes(str) {
    var substrings = str.split("\"");
    if (substrings.length == 1) {
        return str;
    } else {
        return substrings.reduce(function(init, s, i, parent) {
            return init + "\\\"" + s;
        });
    }
}

function extractFragment(url) {
    var idx = url.search(/#.*$/);
    if (idx < 0) {
        return null;
    } else {
        return url.substring(idx+1);
    }
}

// makeAudiotagTimespec : time -> string
// Serialise a time as a string, in a way which can be parsed by audiotag.
function makeAudiotagTimespec(time) {
    return time.getFullYear()
        + ":"+(time.getMonth()+1)
        + ":"+time.getDate()
        + ":"+time.getHours()
        + ":"+time.getMinutes()
        + ":"+(time.getSeconds() + time.getMilliseconds()/1000);
}

// convertMsToMins : integer -> string
// Convert a duration, in milliseconds, to minutes and seconds for display.
function convertMsToMins(ms) {
    var s = Math.round(ms/1000);
    var m = Math.floor(s / 60);
    s = s - m*60;
    return m+'m'+s+'s';
}
