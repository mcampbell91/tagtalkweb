/** this is the java script that creates the timeline **/
/** on the load of the page, it creats a list of all the tags and adds them to the timeline 
 ** if logged in it displays the buttons to add new tags **/


/** CURRENTLY WORKS FOR ADDING AND DELETING TAGS */

/** FIXES SO TO DO 
	* 2. ADD THE AUDIO MANAGER - Skip buttons
	* 3. ADD REFRESH BUTTON TO THE TIMELINE
	* 5. CHANGE FROM GOOGLE TABLE TO JSON
	**/

//this is all the attributes in this file

var title; //of the current lecture
var description; //of the current lecture
var series;
var seriesURL;
var selected; //the number of the currently selected tag
var start; //date object of the start of lecture
var end; //date object for the end of the lecture
var timeline;
var data;
var paused = false;
var currentTime;
var updateTime;
var baseUrl = "/podcasting/track/"; //plus the id of this lecture course and the lecture id
var tagUrl ="/podcasting/event/"; //plus the id of the tag we want the info about

//Global list of all the tags, filled from a get response from the server
//It is in the format Time of tag, *no end time* , tag name, tag description, id, likes
var tags = [[new Date(0,0,0,0), , , 'DEFAULT TAG','id', '0']];

function onLoad(){
	//the main entry point into the page 
	
	//first get info about the lecture 
	//alert("Getting info");
	makeInfoRequest();

	//if a user is logged in then show the tagging buttons
	
 	
	//load the audio file 
	
}

/** BEGIN SECTION - GET SERVER CALLS **/


function postNewTag(){	

	var t; 

	if (paused){
		t = currentTime;
	}
	else{
		t = timeline.getCurrentTime();
	}
	
	var time = formatTime(t);

	postingUrl =  baseUrl + series + "/" + title;
	//alert ("posting url = " + postingUrl);

	var http_request = false;

        if (window.XMLHttpRequest) { // Mozilla, Safari,...
            http_request = new XMLHttpRequest();
            if (http_request.overrideMimeType) {
                http_request.overrideMimeType('text/xml');
                // See note below about this line
            }
        } else if (window.ActiveXObject) { // IE
            try {
                http_request = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                try {
                    http_request = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (e) {}
            }
        }

        if (!http_request) {
            alert('Giving up :( Cannot create an XMLHTTP instance');
            return false;
        }
	

	var tagbox = document.getElementById("tagbox");
	var s = tagbox.elements[0].value;

	tagbox.elements[0].value = "tag...";
		
	
	//date sting yyyy-mm-dd'T'hh:mm:ss
	
	
	//alert("time " + time);
	//var tag = '(event (date "2010-06-21T11:32:13") (tags "' + s + '"))';
	//(title "----") (description "-------")
	
	var tag = '(event (date "' + time + '") (tags "' + s + '"))';

	//alert(tag); 

        http_request.onreadystatechange = function() { handlePost(http_request); };
        http_request.open('POST', postingUrl, true);
    	http_request.setRequestHeader("Content-Type", "text/plain");
    	http_request.setRequestHeader("Accept", "application/json");
        http_request.send(tag);

}


function postNewTagDes(){	
	
	//alert("postNewDesTag");
	var t; 

	if (paused){
		t = currentTime;
	}
	else{
		t = timeline.getCurrentTime();
	}

	var time = formatTime(t);

	postingUrl =  baseUrl + series + "/" + title;
	//alert ("posting url = " + postingUrl);

	var http_request = false;

        if (window.XMLHttpRequest) { // Mozilla, Safari,...
            http_request = new XMLHttpRequest();
            if (http_request.overrideMimeType) {
                http_request.overrideMimeType('text/xml');
                // See note below about this line
            }
        } else if (window.ActiveXObject) { // IE
            try {
                http_request = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                try {
                    http_request = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (e) {}
            }
        }

        if (!http_request) {
            alert('Giving up :( Cannot create an XMLHTTP instance');
            return false;
        }
	

	var tagbox = document.getElementById("tagbox");
	var s = tagbox.elements[0].value;

	tagbox.elements[0].value = "tag...";

	var desbox = document.getElementById("desbox");
	var d = desbox.elements[0].value;
	
	desbox.elements[0].value = 'description...';	

	//date sting yyyy-mm-dd'T'hh:mm:ss
	
	//var tag = '(event (date "2010-06-21T11:32:13") (tags "' + s + '"))';
	//(title "----") (description "-------")
	
	var tag = '(event (date "' + time + '") (tags "' + s + '")(description "' + d + '"))';

	//alert(tag); 

        http_request.onreadystatechange = function() { handlePost(http_request); };
        http_request.open('POST', postingUrl, true);
    	http_request.setRequestHeader("Content-Type", "text/plain");
    	http_request.setRequestHeader("Accept", "application/json");
        http_request.send(tag);

}

function postEditTag(){	

	//this is done by saving the tags details
	//then deleting the existing tag and reposting

	//set the tag to be the new tag if this is not being changed
	var tagbox = document.getElementById("tagbox");
	var s = tagbox.elements[0].value;

	if(s == "tag..."){
		tagbox.elements[0].value = tags[selected][2].replace(" ", "");
	}

	//save the time
	updateTime = tags[selected][0];
	
	deleteTag();
	postNewTagDes();


}


function postLikeTag(){	

	var id = tags[selected][2].replace(" ", "");

	postingUrl = tags[selected][4] + "/" + id;
	
	var http_request = false;

        if (window.XMLHttpRequest) { // Mozilla, Safari,...
            http_request = new XMLHttpRequest();
            if (http_request.overrideMimeType) {
                http_request.overrideMimeType('text/xml');
                // See note below about this line
            }
        } else if (window.ActiveXObject) { // IE
            try {
                http_request = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                try {
                    http_request = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (e) {}
            }
        }

        if (!http_request) {
            alert('Giving up :( Cannot create an XMLHTTP instance');
            return false;
        }
	
	///event/{eventid}/{tag}

	var id1 = tags[selected][4].split("/");
	var id = id1[3];


	//in order to tag this must be json

	//var tag = "{ \"likes\": 1 }";

	//alert(tag); 

	//this is to help debug the mobile app 
	var j = new JSONObject('{"events": [{"start": "2012-09-19T11:15:00", "tags": "one"}]}');

        http_request.onreadystatechange = function() { handlePost(http_request); };
        http_request.open('POST', "http://ptolemy.astro.gla.ac.uk/podcasting/track/a2sr/l01", true);
    	http_request.setRequestHeader("Content-Type", "application/json");
    	http_request.setRequestHeader("Accept", "application/json");
        http_request.send(tag);

}



function makeInfoRequest() {
	//this creates the get response and handles it
	//it uses the results to fill in the table
        var http_request = false;
	
	var s = window.location.search;
	var test =s.split("=");
	var url = test[1];
	series = test [2];
	seriesURL = test [3];

 
        if (window.XMLHttpRequest) { // Mozilla, Safari,...
            http_request = new XMLHttpRequest();
            if (http_request.overrideMimeType) {
                http_request.overrideMimeType('text/xml');
                // See note below about this line
            }
        } else if (window.ActiveXObject) { // IE
            try {
                http_request = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                try {
                    http_request = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (e) {}
            }
        }

        if (!http_request) {
            alert('Giving up :( Cannot create an XMLHTTP instance');
            return false;
        }
        http_request.onreadystatechange = function() { fillInfo(http_request); };
        http_request.open('GET', url, true);
    	http_request.setRequestHeader("Content-Type", "text/plain");
    	http_request.setRequestHeader("Accept", "application/json");
        http_request.send(null);

    }

//get all of the tags to be included
	function getTags(){
		
	//make get tag request now to fill up the tags listting
	//alert("Getting tags");
	
 	var http_request = false;

	if (window.XMLHttpRequest) { // Mozilla, Safari,...
            http_request = new XMLHttpRequest();
            if (http_request.overrideMimeType) {
                http_request.overrideMimeType('text/xml');
                // See note below about this line
            }
        } else if (window.ActiveXObject) { // IE
            try {
                http_request = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                try {
                    http_request = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (e) {}
            }
        }

        if (!http_request) {
            alert('Giving up :( Cannot create an XMLHTTP instance');
            return false;
        }
	
	url = baseUrl + series + "/" + title; //base url + the series name and the recording name
	
	//alert("url =" + url);
        http_request.onreadystatechange = function() { addTags(http_request); };
        http_request.open('GET', url, true);
    	http_request.setRequestHeader("Content-Type", "text/plain");
    	http_request.setRequestHeader("Accept", "application/json");
        http_request.send(null);
	
	}

/** END SECTION **/

/** BEGIN SECTION - SERVER RESPONSE HANDLERS **/

function addTags(http_request) {

        if (http_request.readyState == 4) {
            if (http_request.status == 200) {
		var info = http_request.responseText;
		var taglist = info.split("id");
		var newTags = []; //this is so we can not have duplicates
		
		//alert("Selected all of the tags " + info);
			
		//tags.push([new Date(2013,0,29,11,15), , "hello", "world"]);//add to tag list
	        //newTags.push([new Date(2013,0,29,11,15), , "hello", "world"]);

		//parse for each tag in the list and add to the list of tags
		//the first tag in this list is always the start point so we ignore this
		//thats why i starts at a value of 2
		//push (tag time, , tag, description, url, likes)
		
		//alert(taglist.length);
		for(i=2; i<taglist.length; i++){
			
			var t = taglist[i].split("tag");
			//alert("tag: " + i + " " + t[0]);

			//get the tag name and description
		var temp = t[0].split(":");

						
			temp1 = temp[3].split(",");
			var tag = temp1[0];

			if(tag ==' ""' || tag ==' "'){
				//this is not a real tag
				//so do nothing in this case
			}
			else{ //this is a real tag

				//alert("real tag");

				//alert(i + "is a real tag " + tag);
			     	tag = tag.replace('"', "");
			     	tag = tag.replace('"', "");

				//alert("tag = " + tag);

				var description = temp[4];
				description = description.replace(', "', "");
				description = description.replace('"', "");
						
				//alert("description = " + description);
				//get the date

				var idRaw = temp[2].split(",");
				var urlid = idRaw[0];
				urlid = urlid.replace('"', "");
				urlid = urlid.replace('"', "");

				//alert("url id " + urlid);


				temp = t[3].split("new");
				var d = temp[1].split(", \"");

				//alert(d[0]);
				
				var tagDate = createDate(d[0]);

				//get the likes
				var likes = temp[0].split("likes");
				likes = likes[1].split(",");

				//alert(likes[0]);
				
				var l = likes[0];
				l = l.replace('": ', "");


				tags.push([tagDate, , tag, description,urlid, l]);//add to tag list

				//alert(tags);
				newTags.push([tagDate, , tag, description, urlid, l]);//add to new tag list
				
			}

		    //alert("New Tags " + newTags);
		}
		
		//alert("hello ");
		//alert("New tags are: " + newTags );

		//add them to the timeline
		data.addRows(newTags);
		//alert("got the new tags " + newTags);
		//set the timeline to the correct time for the audio
		
		//alert("start time is: "+start);
		timeline.setVisibleChartRange(start, end);
		//timeline.setCurrentTime(start);
	   	timeline.setCustomTime(start);

		//setTime();

		//timeline.setVisibleChartRangeNow();

		//redraw the timeline
		timeline.redraw();

		//clear newtags
		newTags = [];
		//alert("New tags are: " + newTags );

		
            } else {
                alert('There was a problem with the GET TAGS request.');
            }
        }

    }

    //called from the first request
    //gets the details of the given series
    //these then get a request to get all of the previous lectures
    function fillInfo(http_request) {

        if (http_request.readyState == 4) {
            if (http_request.status == 200) {
		var info = http_request.responseText;
		
		//we now parse the string using string.split()
		//I can't get it to parse with jquery.parseJSON 
		//so I shall try getting this working later

		//we want to store create a list of tags
		//and fill in the course info on this lecture
		//this happens regardless of weither the user is logged in or not

		var infoA = info.split(': "');
		
		var temp = infoA[3].split('", "');
	 	title = temp[0];

		temp = infoA[4].split('", "');
		description = temp[0];
		

		//the tag included in this marks the start point of the audio 
		//to get a list of all the tags we make a different request
		//we now need to get the start and end of the audio timeline
		
		if(infoA[6].indexOf("new") == -1){
			var dates = infoA[7].split("new");

			//call the method create date, this makes a new date object and returns
			//called for both start and end date
		
			start = createDate(dates[1]);
			end = createDate(dates[2]);
		}
		else{
			var dates = infoA[6].split("new");

			//call the method create date, this makes a new date object and returns
			//called for both start and end date
		
			start = createDate(dates[1]);
			end = createDate(dates[2]);
		}

			
		addText();

		getTags();
		
            } else {
                alert('There was a problem with the FILL INFO request.');
            }
        }

    }



function handlePost(http_request)
{
     if (http_request.readyState == 4) {
             if (http_request.status == 200) {
		var info = http_request.responseText;
		//alert("request completed successfully." + info);

		//add the tag to the list of tags
		alert("Please ensure you are logged in before adding a tag. " + http_request.responseText);
            }
        }

}


/** END SECTION **/

/** BEGIN SECTION - HELPER METHODS **/

//generates the date from the server string 
//its in the format Date(YYYY, M, DD, HH , MM, SS, MSMS)
function createDate(date){
	
	//alert("in create date " + date);
	var nums = date.split(",");
	//alert(" dates: " + nums);
	
	//get all the numbers to create a date object
	
	var temp = nums[0].split("(");
	var year = parseInt(temp[1]);
	
	//alert("year =" + year + " " + typeof year);

	var month = parseInt(nums[1]);
	var day = parseInt(nums[2]);
	var hour = parseInt(nums[3]);
	var mins = parseInt(nums[4]);
	var secs = parseInt(nums[5]);

	var temp = nums[6].split(")");
	var msecs = temp[0]; 

	//create the new date
	var ndate = new Date(year, month, day, hour, mins, secs, msecs);
	var date = new Array(year, month, day, hour, mins,secs, msecs);
	
	return ndate;
}

//add text to the page
function addText(){
	
	
	var el = document.getElementById("lecturename");
	el.innerHTML = title;

	var el = document.getElementById("lectureinfo");
	el.innerHTML = description;

	//el = document.getElementById("back"); 
        //el.setAttribute('href',seriesURL);
	//el.innerHTML = "Back to course page."; 

}


        // Called when the timelineualization API is loaded.
        function drawVisualization() {
            // Create and populate a data table.

	    //this is to be changed to json soon
	    //this will only contain the tag number and time 
	
            data = new google.visualization.DataTable();
            data.addColumn('datetime', 'start');
            data.addColumn('datetime', 'end');
            data.addColumn('string', 'content');
	    data.addColumn('string', 'description');
	    data.addColumn('string', 'id');
	    data.addColumn('string', 'likes');
		

            function addRow(startDate, endDate, content, backgroundColor, borderColor)
            {
                // we make our own customized layout for the events

                if (backgroundColor == undefined)
                    backgroundColor = "yellow";
                if (borderColor == undefined)
                    borderColor = "gold";

                var fill = endDate ? "pink" : "yellow";
                var div = '<div style="background-color:' + backgroundColor +
                        '; border:1px solid ' + borderColor + ';padding:5px;">' +
                        content + '</div>';

                data.addRows([
                    [startDate, endDate, div]
                ]);
            }
	     
	    data.addRows(tags);
	    
	    document.getElementById('container').value = data.getNumberOfColumns();

	    var sd = new Date(2010, 5, 21, 11, 0);

	   
            // specify options
            var options = {
                width:     "90%",
                height:    "auto",
		start: sd,
		end: new Date(2010, 5, 21, 12, 0),
		showCurrentTime: true,
		showCustomTime: true,
		zoomable: true,
		showNavigation: true,
                style:    "box"    // optional. choose "dot" (default), "box", or "none"
            };

            // Instantiate our table object.
            timeline = new links.Timeline(document.getElementById('mytimeline'));

            // Attach event listeners
            google.visualization.events.addListener(timeline, 'select', onselect);
            google.visualization.events.addListener(timeline, 'rangechange', onrangechange);

            // Draw our table with the data we created locally.
            timeline.draw(data, options);

            // Set the scale by hand. Autoscaling will be disabled.
            // Note: you can achieve the same by specifying scale and step in the
            // options for the timeline.
            //timeline.setScale(links.Timeline.StepDate.SCALE.DAY, 1);
        }

        // adjust start and end time.
        function setTime() {
            if (!timeline) return;

            var newStartDate = start;
            var newEndDate   = end;
            timeline.setVisibleChartRange(newStartDate, newEndDate);
	    //timeline.setVisibleChartRangeNow();
            timeline.redraw();
        }

        function onrangechange() {
            // adjust the values of startDate and endDate
            var range = timeline.getVisibleChartRange();
            document.getElementById('startDate').value = dateFormat(range.start);
            document.getElementById('endDate').value   = dateFormat(range.end);

	   //set to be the centre of the timeline
	   //timeline.setCustomTimeNow();
        }
	
	

        function onselect() {
            var sel = timeline.getSelection();
            if (sel.length) {
                if (sel[0].row != undefined) {
                    var row = sel[0].row;
                       	selected = row;
		        hb = new HelpBalloon({
			title: 'Tag Selected: ' + tags[row][2],
			content: 'Tag Desription: ' + tags[row][3] + "\n"
				+ '\n Likes: ' + tags[row][5],
			icon: $('info'),
			fixedPosition: HelpBalloon.POS_TOP_LEFT
			});
		hb.show();
                }
            }
        }

        // Format given date as "yyyy-mm-dd hh:ii:ss"
        // @param datetime   A Date object.
        function dateFormat(date) {
            datetime =   date.getFullYear() + "-" +
                    ((date.getMonth()   <  9) ? "0" : "") + (date.getMonth() + 1) + "-" +
                    ((date.getDate()    < 10) ? "0" : "") +  date.getDate() + " " +
                    ((date.getHours()   < 10) ? "0" : "") +  date.getHours() + ":" +
                    ((date.getMinutes() < 10) ? "0" : "") +  date.getMinutes() + ":" +
                    ((date.getSeconds() < 10) ? "0" : "") +  date.getSeconds()
            return datetime;
        }
	

//These are the fuctions called when a nw tag is submitted
//either a tag on its own or a tag and a description
//they both call the same post method, they are only seperate handlers so that a tag with description isn't added twice

function postTag(){

	var s =  document.getElementById("tagbox").getAttribute("class");

	//alert(s);

	if(s == "tooltip"){
		//called when return button is pressed in the tag box to add a tag only
		alert("posting new tag");
		postNewTag();
	}
	else{
		postEditTag();
		document.getElementById("tagbox").setAttribute("class", "tooltip");		
	}

	
        //alert ("Adding tag .....");
	return false;
}


function postBoth(){
	//called when return is pressed in the description box

	var s = document.getElementById("tagbox").getAttribute("class");

	if(s == "tooltip"){
		//alert("posting new tag");
		postNewTagDes();
	}
	else{	
		//alert("posting edit");
		postEditTag();
		document.getElementById("tagbox").setAttribute("class", "tooltip");
		document.getElementById("desbox").setAttribute("class", "defaultValue");
	}
	return false;
}


 function likeTag() {
		postLikeTag();
			
		}

 function editTag() {
	//alert("I edited a tag from js");
	
	document.getElementById("tagbox").setAttribute("class", "editValue");
	document.getElementById("desbox").setAttribute("class", "editValue");

	alert("You can now edit the selected tag using the tag and description boxes.");

	//document.getElementById("tagbox").setAttribute("class", "defaultValue");

		//post to (event (id "=---"))
		}


 function deleteTag() {
			//alert("I deleted a tag from js");

	var http_request = false;

	if (window.XMLHttpRequest) { // Mozilla, Safari,...
            http_request = new XMLHttpRequest();
            if (http_request.overrideMimeType) {
                http_request.overrideMimeType('text/xml');
                // See note below about this line
            }
        } else if (window.ActiveXObject) { // IE
            try {
                http_request = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                try {
                    http_request = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (e) {}
            }
        }

        if (!http_request) {
            alert('Giving up :( Cannot create an XMLHTTP instance');
            return false;
        }


	var url = tags[selected][4];

	//alert("deleting url " + url);
	http_request.onreadystatechange = function() {
        if (http_request.readyState == 4) {
            alert("Deleted!");
        }
    };
    http_request.open('DELETE', url, true);
    http_request.send();
		}

 function displayInfo() {

	hb = new HelpBalloon({
			title: 'Tag Selected: ',
			content: 'Tag Desription: ',
			icon: $('info'),
			fixedPosition: HelpBalloon.POS_TOP_LEFT
			});
	hb.show();
		}

function formatTime(timeinfo){
	//Current format of the string - Tue Jan 29 2013 11:03:24 GMT+0000 (GMT)
	//Desired format - "2010-06-21T11:32:13" - yyyy-mm-ddThh:mm:ss
	
	var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	var monthNum = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];

	var lst1 = String(timeinfo);
	var lst2 = lst1.split(" ");

	//alert(lst2);

	var m = lst2[1];
	//get the correct month number
	//TO ADD LATER

	var found = false;
	var x = 0;
	var r = 0;
	
	while(found == false) {
		if (m == months[x]){
			//we have found the correct month
			found = true;
			r = x;
		}
		x++;
	}

	//var month = "06";

	var newTime = lst2[3]+"-"+monthNum[r]+"-"+lst2[2]+"T"+lst2[4];
	//alert(newTime);
	return newTime;
}

function reloadTimeline(){

	alert("reloading the timeline");
	
	//clear of the old tags
	tags = [[new Date(0,0,0,0), , , 'DEFAULT TAG','id', '0']];
	data = new google.visualization.DataTable();
        data.addColumn('datetime', 'start');
        data.addColumn('datetime', 'end');
        data.addColumn('string', 'content');
	data.addColumn('string', 'description');
	data.addColumn('string', 'id');
	data.addColumn('string', 'likes');
	
	getTags();
}

function soundPlay(){
	//alert("trying to play sound");

	paused = false;
	var url = "audio/" + series.replace("_"," ") + "/" + title + ".mp3";
	//alert(url);

	audiotagSound = soundManager.createSound({ id: "test", // use URL as ID, easing later retrieval
                                                       url: url,
                                                       preferFlash: false
                                                     });
	
	audiotagSound.play();
	
	if(audiotagSound.position == null){
	timeline.setCurrentTime(start);
	}
	else{
		var ms = start.getTime();
		var d = new Date (0,0,0,0,0);
		d.setTime(ms+audiotagSound.position);
		timeline.setCurrentTime(d);
	}
		
	//alert(audiotagSound.position);
}

function pauseSound(){
	//alert("pausing sound");
	audiotagSound.pause();

	currentTime = timeline.getCurrentTime();
	paused = true;
	timeline.setCurrentTime(new Date(0,0,0,0,0));
	
}

function test(){
	//alert("testing.... ");
}
/** END SECTION **/

