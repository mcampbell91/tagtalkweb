DEST=/home/melissa/audiotag/install
WEBROOT=$(DEST)/web-root/htdocs

install: tagtalkfiles
	rm -Rf $(WEBROOT)/podcasting/tagtalkfiles
	cp -R tagtalkfiles $(WEBROOT)/podcasting
	cp index.html $(WEBROOT)/podcasting/index.html 
